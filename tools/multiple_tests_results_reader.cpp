#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(){

  ifstream results;
  results.open("multiple_results.txt");
  string line;

  int correct = 0, uncorrect = 0;

  getline(results,line);

  while (!(results.eof())){
   
    if (line == "0") correct ++;
    else uncorrect++;
    getline(results,line);

  } 

  cout << "----------------------" << endl;
  cout << "----------------------" << endl << endl;

  cout << "Multiple tests result : \n\n";
  cout << "Passed : " << correct << endl;
  cout << "Failed : " << uncorrect << endl << endl;

  cout << "----------------------" << endl;
  cout << "----------------------" << endl << endl;


  return 0;

}
