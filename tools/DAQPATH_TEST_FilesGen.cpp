/*
  This script produces all needed files to test daqpath v2 example designs
*/

#include <iostream>
#include <random>
#include <fstream>
#include <string>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <cstdlib>
#include <ctime>

using namespace std;

const string SW_version = "\nFilesGenerator tool for IPBUS - version 2.0 \n\n";

//  Number of channels
unsigned int NUM_CHANNELS = 16;  //default value 16

//  Number of generated events
unsigned int NUM_EVENTS = 5;    // 5 events dafault value

//  Used clock period
long double CLOCK_PERIOD = 0.000000010; //100 MHz default

//  hdr enable
unsigned int hdr_en = 1;  //default value 1

//  input data width
unsigned int input_data_width = 64;  //default value 64

//  ch_mask
unsigned int ch_mask_low, ch_mask_high = 0;  //default value 0x00000000

//  Mean L1A trigger rate (<750 KHz)
const long double MEAN_L1A_RATE = 750000;

//  Number of output IPBUS FIFOs
const int NUM_OUT_LINKS = 1;

const int NUM_CH_REGISTERS = 4;

const int NUM_MAIN_REGISTERS = 8;

bool errors = false;

void Print_opt(){
  cout << endl << "usage : DAQPATH_TEST_FilesGen [options] <NUM_CHANNELS> <NUM_EVENTS> <FW_FREQUENCY> <HDR_EN> <CH_MASK> <DAQ_INPUT_DATA_WIDTH>" << endl << endl;
  cout << "options : -h,--help     (print usage message)" << endl;
  cout << "          -err          (insert event errors in data generation)" << endl << endl;
  cout << "Arguments : "<< endl;
  cout << "1) Number of channels of the architecture to feed (2 - 64)" << endl;
  cout << "2) Number of events to generate (1 - 100)" << endl;
  cout << "3) Readout architecture simulation clock frequency reference (MHz) (10 - 500)" << endl;
  cout << "4) Header_enable (1/0) , if 1 header word is added at the beginning of every channel packet" << endl;
  cout << "5) Channel mask (max 64 bits) vector in hex code (0000000000000000 - ffffffffffffffff) , if ch_mask(i) = 1, channel(i) will be skipped - MSB at left" << endl;
  cout << "6) Daqpath input data bitwidth (allowed 8,16,32,64)" << endl << endl;
  return;
}

void Generate_Address_Table(){

  // Address table xml file generation
  remove("DTC_DAQPATH.xml");
  ofstream Addr_tab;
  Addr_tab.open("DTC_DAQPATH.xml", ios::out | ios::app);

  stringstream ss_addr;
 
  ss_addr << "<node>\n";

  //  Main CSREGS
  ss_addr << "  <node id=\"MAIN_CSR\" address=\"0x0\" fwinfo=\"endpoint;width=4\">\n";
  for (int i = 0; i < NUM_MAIN_REGISTERS; ++i)
    ss_addr << "    <node id=\"C" + to_string(i) + "\" address=\"0x" << std::hex << std::setw(1) << std::setfill('0') << i  << "\" />\n";
  for (int i = 0; i < NUM_MAIN_REGISTERS; ++i) 
    ss_addr << "    <node id=\"S" + to_string(i) + "\" address=\"0x" << std::hex << std::setw(1) << std::setfill('0') << i + NUM_MAIN_REGISTERS << "\" />\n";
  ss_addr <<"  </node>\n\n";

  //  DAQPATH CSREGS
  ss_addr << "  <node id=\"DAQPATH_CSR\" address=\"0x10\" fwinfo=\"endpoint;width=4\">\n";
  for (int i = 0; i < 3; ++i)
    ss_addr << "    <node id=\"C" + to_string(i) + "\" address=\"0x" << std::hex << std::setw(1) << std::setfill('0') << i  << "\" />\n";
  for (int i = 0; i < 3; ++i)
    ss_addr << "    <node id=\"S" + to_string(i) + "\" address=\"0x" << std::hex << std::setw(1) << std::setfill('0') << i + 3 << "\" />\n";
  ss_addr <<"  </node>\n\n";  

  //  Channel CSRegs  
  ss_addr << "  <node id=\"CH_CSR\" address=\"0x100000\" fwinfo=\"endpoint;width=20\">\n";
  for (int i = 0; i < NUM_CHANNELS; ++i){
    ss_addr << "    <node id=\"CH" + to_string(i) + "\" address=\"0x" <<  std::hex << std::setw(4) << std::setfill('0') << i*0x100 << "\">\n";
    for (int j = 0; j < NUM_CH_REGISTERS ; ++j)
      ss_addr << "      <node id=\"C" + to_string(j) + "\" address=\"0x" << std::hex << std::setw(1) << std::setfill('0') << j << "\" />\n";
    for (int j = 0; j < NUM_CH_REGISTERS ; ++j) 
      ss_addr << "      <node id=\"S" + to_string(j) + "\" address=\"0x" << std::hex << std::setw(1) << std::setfill('0') << j+NUM_CH_REGISTERS << "\" />\n";
    ss_addr << "    </node>\n\n";
  }
  ss_addr << "  </node>\n\n";

  //  IPB INPUT FIFOs
  ss_addr << "  <node id=\"IN\" address=\"0x200000\" fwinfo=\"endpoint;width=20\">\n";
  for (int i = 0; i < NUM_CHANNELS; ++i){
    ss_addr << "    <node id=\"CH" + to_string(i) + "\" address=\"0x" <<  std::hex << std::setw(4) << std::setfill('0') << i*0x100 << "\">\n";
    ss_addr << "      <node id=\"DWL_FIFO\" address=\"0x0\" mode=\"non-incremental\" size=\"1024\" permission=\"w\"" << " />\n";
    ss_addr << "      <node id=\"DWH_FIFO\" address=\"0x1\" mode=\"non-incremental\" size=\"1024\" permission=\"w\"" << " />\n";
    ss_addr << "      <node id=\"NW_FIFO\" address=\"0x2\" mode=\"non-incremental\" size=\"512\" permission=\"w\"" << " />\n";
    ss_addr << "      <node id=\"ID_FIFO\" address=\"0x3\" mode=\"non-incremental\" size=\"512\" permission=\"w\"" << " />\n";
    ss_addr << "      <node id=\"TS_FIFO\" address=\"0x4\" mode=\"non-incremental\" size=\"512\" permission=\"w\"" << " />\n";
    ss_addr << "    </node>\n\n";
  }
  ss_addr << "  </node>\n\n";
  
  ss_addr << "  <node id=\"OUT\" address=\"0x300000\" fwinfo=\"endpoint;width=20\">\n";
  for (int i = 0; i < NUM_OUT_LINKS; ++i){
    ss_addr << "    <node id=\"LINK" + to_string(i) + "\" address=\"0x" <<  std::hex << std::setw(5) << std::setfill('0') << i*0x10000 << "\">\n";
    ss_addr << "      <node id=\"DWL_FIFO\" address=\"0x0\" mode=\"non-incremental\" size=\"1024\" permission=\"r\"" << " />\n";
    ss_addr << "      <node id=\"DWH_FIFO\" address=\"0x1\" mode=\"non-incremental\" size=\"1024\" permission=\"r\"" << " />\n";
    ss_addr << "      <node id=\"IDNW_FIFO\" address=\"0x2\" mode=\"non-incremental\" size=\"512\" permission=\"r\"" << " />\n";
    ss_addr << "    </node>\n\n";
  }
  ss_addr << "  </node>\n\n";
  
  Addr_tab << ss_addr.str();
  Addr_tab << "</node>\n";

  return;

};

int main( int argc, char const *argv[] )
{

  cout << SW_version;

  // Cmd line arguments parsing ------------------------------------------------------------------

  if (argc > 1){

    bool options = (argv[1][0] == '-') ? true : false;
    
    if(options){
      string opt = argv[1];      
      if(opt == "-h\0" || opt == "--help\0") {
        Print_opt();
        return 0;
      }
      else if(opt == "-err\0"){
        errors = true; 
      }  
      else{
        Print_opt();
        return 0;
      }
    }
    
    int op = options ? 1 : 0;

    if (argc != 7 + op) {
      cout << "Incorrect usage!" << endl << endl;
      Print_opt();
      return 0;
    }

    if(atoi(argv[1+op]) >= 2 && atoi(argv[1+op]) <= 64)
      NUM_CHANNELS = atoi(argv[1+op]);             
      else{
        cout << "Invalid argument for the number of channels\n";
        Print_opt();
        return 0;
      }

    if(atoi(argv[2+op]) >= 1 && atoi(argv[2+op]) <= 100)
      NUM_EVENTS = atoi(argv[2+op]);
      else{
        cout << "Invalid argument for the number of events\n";
        Print_opt();
        return 0;
      }

    if(atoi(argv[3+op]) >= 10 && atoi(argv[3+op]) <= 500)
      CLOCK_PERIOD = (long double) 1.0/(atof(argv[3+op])*1000000.0);
      else{
        cout << "Invalid argument for the FW frequency\n";
        Print_opt();
        return 0;
      }

    if(atoi(argv[4+op]) <= 1 && atoi(argv[4+op]) >= 0)
      hdr_en = atoi(argv[4+op]);
      else{
        cout << "Invalid argument for hdr_en\n";
        Print_opt();
        return 0;
      }

    if(strlen(argv[5+op]) >= 1 && strlen(argv[5+op]) <= 16){
      size_t length = strlen(argv[5+op]);
      if (length <= 8){
        ch_mask_low = stol(argv[5+op], NULL, 16);
        ch_mask_high = 0x0;
      }
      else{
        char ch_low[9];
        char* ch_high = new char[length-8+1];
        memcpy(ch_low, &argv[1][length-8], 8);
        memcpy(ch_high, &argv[1][0], length-8);
        ch_mask_low = stol(ch_low, NULL, 16);
        ch_mask_high = stol(ch_high, NULL, 16);
      }
    }
    else{
      cout << "Invalid argument for ch_mask : min 1bit, max 64 bits\n";
      Print_opt();
      return 0;
    }

    if( atoi(argv[6+op]) != 8 && atoi(argv[6+op]) != 16 && atoi(argv[6+op]) != 32 && atoi(argv[6+op]) != 64 ){
      cout << "Invalid argument for input data width : allowed 8,16,32,64\n";
      Print_opt();
      return 0;
    }
    else{
      input_data_width = atoi(argv[6+op]);
    }

    string er = (errors) ? "with" : "without";

    cout << "Generating " << NUM_EVENTS << " events on " << NUM_CHANNELS << " channels " << er << " packet errors. Using a readout clock frequency value of " << (double) 1.0/(CLOCK_PERIOD*1000000) << " MHz" << endl;
  }
  else{
    cout << "Incorrect usage!" << endl << endl;
    Print_opt();
    return 0;
  }
  
  // ----------------------------------------------------------------------------------------------------

  //  Initialisation-------------------------------------------------------------------------------------

  //  Initialising random engine
  default_random_engine rnd_gen{static_cast<long unsigned int>(time(0))};

  //  Initialising exponential distribution for random event interval generation
  exponential_distribution<double> event_interval (MEAN_L1A_RATE);

  //  Initialising normal distribution for random channel offset generation
  normal_distribution<double> offset (0.0, 1.5);

  //  Initialising uniform distribution for random channel words number per event generation
  uniform_int_distribution<int> word_num(0,10);

  //  Initialising uniform distribution for random channel words payload generation
  uniform_int_distribution<unsigned int> word(0,0xFFFF);   
  
  //  Generate address table
  Generate_Address_Table();

  int *channel_TS, *random_delay, *words_num_ev;
  channel_TS = new int[NUM_CHANNELS];
  random_delay = new int[NUM_CHANNELS];
  words_num_ev = new int[NUM_EVENTS];

  int event_index = 1, *channel_words, *channel_words_delay, *last_word_TS;
  channel_words = new int[NUM_CHANNELS];
  channel_words_delay = new int[NUM_CHANNELS];
  last_word_TS = new int [NUM_CHANNELS];

  int broken_channel, *missing_event;
  broken_channel = -1;                     //if broken_channel = n ... No events will arrive on channel n
  missing_event = new int[NUM_CHANNELS];   //if missing event[n] = m ... event m on channel n is missing

  long double EV_INTERVAL;
  int CLOCK_CYCLES = 0; 

  //  ---------------------------------------------------------------------------------------------------


  // Old Files Removal-----------------------------------------------------------------------------------

  remove("Data_out_ref.txt");
  remove("HW_IPBUS_TEST_DATA.txt");

  // -----------------------------------------------------------------------------------------------------


  //  Data files generation ------------------------------------------------------------------------------ 
  
  //  Set Channel latency fixed offsets
  for( int i = 0; i < NUM_CHANNELS; i++ )
    channel_TS[i] = (int) offset(rnd_gen);

  cout << endl;
  
  for( int i = 0; i < NUM_CHANNELS; i++ )
    cout << "latency fixed channel_offset["<<i<<"] = " << channel_TS[i] << endl;

  cout << endl << endl;  
  
  //initialisation
  for(int i = 0; i<NUM_CHANNELS; i++){
    channel_words_delay[i] = 0;
    last_word_TS[i] = 0;
    missing_event[i] = 0; 
  }

  //packet error insertion
  if (errors) {
    srand (time(NULL));
    broken_channel = rand()%(NUM_CHANNELS+1) - 1;                    // from -1(no broken channels) to NUM_CHANNELS-1
    missing_event[rand()%NUM_CHANNELS] = (rand()%NUM_EVENTS) + 1;         
  }
  
  ofstream Data_out;
  Data_out.open("Data_out_ref.txt", ios::out | ios::app );

  ofstream IPBUS_file;
  ofstream IPBUS_HW_TEST_DATA_FILE;
  stringstream ipb_hw_test_data_ss;

  IPBUS_HW_TEST_DATA_FILE.open("HW_IPBUS_TEST_DATA.txt", ios::out | ios::app );
  IPBUS_HW_TEST_DATA_FILE << NUM_CHANNELS << " " << NUM_EVENTS << " " << endl;
  
  while( event_index <= NUM_EVENTS )
  {
    // Interval between a certain event and the next one (exponential distribution with 750 KHz mean rate)
    EV_INTERVAL = event_interval(rnd_gen);
    CLOCK_CYCLES = EV_INTERVAL/CLOCK_PERIOD;
    //At least one clock cycle between two events
    if (CLOCK_CYCLES < 1) CLOCK_CYCLES = 1;

    //To avoid initial negative time_stamps
    for( int i = 0; i<NUM_CHANNELS ; i++)
        if( event_index == 1 && CLOCK_CYCLES + channel_TS[i] <= 0  ) CLOCK_CYCLES += -channel_TS[i] ;
    
    //To avoid FIFO reset problems
    if(event_index == 1 && CLOCK_CYCLES < 50)
      CLOCK_CYCLES += 50;

    //To manage the case when the words of the previous event are > than the clock cycles until the next event arrives
    if (event_index > 1)
    {
      for(int i = 0; i < NUM_CHANNELS ; i++){
        if (last_word_TS[i] >= channel_TS[i] + CLOCK_CYCLES ) channel_words_delay[i] = last_word_TS[i] - channel_TS[i] - CLOCK_CYCLES ;
        else channel_words_delay[i] = 0;
      }
    }

    //for debugging
    cout << "INTERVAL BETWEEN EVENTS = " << EV_INTERVAL;
    cout << endl << "CLOCK CYCLES ("<< (double) 1.0/(CLOCK_PERIOD*1000000)<<" MHz) = " << CLOCK_CYCLES << endl << endl;

    int event_words_num = 0;
    int bits_count = 0;
    
    //idnw stringstream
    stringstream idnw;
    //output reference file stringstream
    stringstream ss_out;

    // for cycle to write the files
    for(int i = 0; i < NUM_CHANNELS; i++)
    {
      //control file stringstream
      stringstream ss_control;
      // data file stringstream
      stringstream ss;

      channel_TS[i] += CLOCK_CYCLES + channel_words_delay[i];
      //words number generated by uniform distribution
      channel_words[i] = word_num(rnd_gen);
      //channel event delay proportional (equal so far) to words number
      random_delay[i] = channel_words[i];
      channel_TS[i] += random_delay[i];
      
      unsigned int shift = (NUM_CHANNELS < 32) ? i : i-32;
      unsigned long int ch_mask = (i<32) ? ch_mask_low : ch_mask_high; 
      bool is_masked = (ch_mask & 1<<shift) ? true : false;
      
      if ( (i != broken_channel) && (missing_event[i] != event_index) )
      {
        
        if(hdr_en == 1 && !is_masked) {
          if (input_data_width == 64){
            ss_out << "ffffffffff" << std::hex << std::setw(2) << std::setfill('0') << i ;
            bits_count += 48;
          }
          else {
            ss_out << "ff" << std::hex << std::setw(2) << std::setfill('0') << i ;
            bits_count += 16;
          }
          if (bits_count == 64){
            ss_out << '\n';
            event_words_num++;
            bits_count = 0;
          }
        }
        ss_control << std::hex << std::setw(8) << std::setfill('0') << channel_TS[i] << ' ' ;
        ss_control << std::hex << std::setw(4) << std::setfill('0') << event_index << ' ' ;

        if(channel_words[i] == 0)
        {
          ss_control << std::hex << std::setw(4) << std::setfill('0') << 0x1  << '\n' ;
          if (hdr_en == 1 && !is_masked) {
            ss_out << std::hex << std::setw(4) << std::setfill('0') << 0x1;
            bits_count+=16;
            if (bits_count == 64){
              ss_out << '\n';
              bits_count = 0;
              event_words_num++;
            }
          }
        }
        else
        {
          ss_control << std::hex << std::setw(4) << std::setfill('0') << channel_words[i]  << '\n' ;
          if (hdr_en == 1 && !is_masked) {
            ss_out << std::hex << std::setw(4) << std::setfill('0') << channel_words[i];
            bits_count+=16;
            if (bits_count == 64){
              ss_out << '\n';
              event_words_num++;
              bits_count = 0;
            }
          }
        }

        //time stamp of the last word written
        last_word_TS[i] = channel_TS[i];

        if(channel_words[i]==0)
        {
          ss << std::hex << std::setw(10) << std::setfill('0') << 0 ;
          ss << std::hex << std::setw(2) << std::setfill('0') << event_index;
          ss << std::hex << std::setw(2) << std::setfill('0') << i;
          ss << std::hex << std::setw(2) << std::setfill('0') << 0 << '\n';

          if (!is_masked) {
            switch(input_data_width){
              case 64 :
                ss_out << std::hex << std::setw(10) << std::setfill('0') << 0 ;
                ss_out << std::hex << std::setw(2) << std::setfill('0') << event_index;
                ss_out << std::hex << std::setw(2) << std::setfill('0') << i;
                ss_out << std::hex << std::setw(2) << std::setfill('0') << 0;
              break;

              case 32 :
                ss_out << std::hex << std::setw(2) << std::setfill('0') << 0 ;
                ss_out << std::hex << std::setw(2) << std::setfill('0') << event_index;
                ss_out << std::hex << std::setw(2) << std::setfill('0') << i;
                ss_out << std::hex << std::setw(2) << std::setfill('0') << 0;
              break;

              case 16 :
                ss_out << std::hex << std::setw(2) << std::setfill('0') << i;
                ss_out << std::hex << std::setw(2) << std::setfill('0') << 0;
              break;

              case 8 :
                ss_out << std::hex << std::setw(2) << std::setfill('0') << 0;
              break;
            }
            
            bits_count+=input_data_width;

            if (bits_count == 64){
              ss_out << '\n';
              event_words_num++;
              bits_count = 0;
            }            
          }

          last_word_TS[i]++;
        }
        else
        {
          for(int j = 1; j<=channel_words[i]; j++)
          { 
            ss << std::hex << std::setw(10) << std::setfill('0') << 0 ;
            ss << std::hex << std::setw(2) << std::setfill('0') << event_index;
            ss << std::hex << std::setw(2) << std::setfill('0') << i;
            ss << std::hex << std::setw(2) << std::setfill('0') << j << '\n';

            if (!is_masked) {
              switch(input_data_width){
                case 64 :
                  ss_out << std::hex << std::setw(10) << std::setfill('0') << 0 ;
                  ss_out << std::hex << std::setw(2) << std::setfill('0') << event_index;
                  ss_out << std::hex << std::setw(2) << std::setfill('0') << i;
                  ss_out << std::hex << std::setw(2) << std::setfill('0') << j;
                break;

                case 32 :
                  ss_out << std::hex << std::setw(2) << std::setfill('0') << 0 ;
                  ss_out << std::hex << std::setw(2) << std::setfill('0') << event_index;
                  ss_out << std::hex << std::setw(2) << std::setfill('0') << i;
                  ss_out << std::hex << std::setw(2) << std::setfill('0') << j;
                break;

                case 16 :
                  ss_out << std::hex << std::setw(2) << std::setfill('0') << i;
                  ss_out << std::hex << std::setw(2) << std::setfill('0') << j;
                break;

                case 8 :
                  ss_out << std::hex << std::setw(2) << std::setfill('0') << j;
                break;

                default :
                  ss_out <<"";
                break;
              }
            
              bits_count+=input_data_width;

              if (bits_count == 64){
                ss_out << '\n';
                event_words_num++;
                bits_count = 0;
              }            
            }          
          
            last_word_TS[i]++;
          }
        }
      }      
      else{
        last_word_TS[i] = channel_TS[i];
        if (!is_masked) {
          switch(input_data_width){
            case 64 :
              ss_out << "ffffffffffff" << std::hex << std::setw(2) << std::setfill('0') << i << "ff";
              bits_count +=64;
            break;

            case 32 :
              ss_out << "ffff" << std::hex << std::setw(2) << std::setfill('0') << i << "ff";
              bits_count +=32;
            break;

            case 16 :
              ss_out << std::hex << std::setw(2) << std::setfill('0') << i << "ff";
              bits_count +=16;
            break;

            case 8 :
              ss_out << std::hex << std::setw(2) << std::setfill('0') << i << "ff";
              bits_count +=16;
            break;

            default :
              ss_out <<"";
            break;
          }

          if (bits_count == 64){
            ss_out << '\n';
            event_words_num++;
            bits_count = 0;
          } 
        }      
        ss_control << "-- Missing Event\n";
      }

      ipb_hw_test_data_ss << ss_control.str();
      if (i != broken_channel) {
        ipb_hw_test_data_ss << ss.str();
      }
    }
    
    if(bits_count != 0) {
        unsigned int missing_nibbles = (64-bits_count)/4; 
        ss_out << std::hex << std::setw(missing_nibbles) << std::setfill('0') << 0 << endl; 
        event_words_num++;
      }

    words_num_ev[event_index-1] = event_words_num; 
    idnw << '#' << std::hex << std::setw(4) << std::setfill('0') << event_index++;
    idnw << std::hex << std::setw(4) << std::setfill('0') << event_words_num << '\n';
    Data_out << ss_out.str();
    Data_out << idnw.str();    
  }

  Data_out.close();

  IPBUS_HW_TEST_DATA_FILE << ipb_hw_test_data_ss.str();
  IPBUS_HW_TEST_DATA_FILE.close();  

  // -----------------------------------------------------------------------------------------------------

  delete[] channel_TS;
  delete[] last_word_TS;
  delete[] channel_words;
  delete[] channel_words_delay;
  delete[] random_delay;
  delete[] words_num_ev;
  
  return 0;
}