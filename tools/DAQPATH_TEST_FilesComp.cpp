#include <iostream>
#include <random>
#include <fstream>
#include <string>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <sys/stat.h>
#include <ctime>

using namespace std;

void Print_opt(){
  cout << endl << "The script is used to compare the output test file obtained with data coming from HW test with the reference file with expected data for the test" << endl;
  cout << endl << "usage : DAQPATH_TEST_FilesComp [-h,--help] <Arguments>" << endl << endl;
  cout << "options : -h,--help       (print usage message)" << endl;
  cout << "Arguments : "<< endl;
  cout << "  1)   Output data filename" << endl;
  cout << "  2)   Reference filename" << endl;
  return;
}

const string SW_version = "\nFilesComparator tool for IPBUS - version 2.0\n\n";

int REF_EVENTS = 0, OUT_EVENTS = 0;
ifstream Ref_file, Out_file;
string filename, filename_ref; 

int main(int argc, char const *argv[])
{
  cout << SW_version;

  if (argc > 1){

  bool options = (argv[1][0] == '-') ? true : false;
  
  if(options){
    string opt = argv[1];      
    if(opt == "-h\0" || opt == "--help\0") {
      Print_opt();
      return 0;
    }
    else{
      Print_opt();
      return 0;
    }
  }

  if (argc != 3) {
    cout << "Incorrect usage!" << endl << endl;
    Print_opt();
    return 0;
  } 
   
  filename = argv[1];
 
  Out_file.open(filename, ios::in);
  if(!Out_file.is_open()){
    cout << "\nCannot find "<< filename <<" file, please check if it's a correct file : aborted\n";
    return 0;
  }  
  
  filename_ref = argv[2];
  
  Ref_file.open(filename_ref, ios::in);
  if(!Ref_file.is_open()){
    cout << "\nCannot find " << filename_ref << " file, please check if it's a correct file : aborted\n";
    return 0;
  }

  cout << "Output data file : " << filename << endl << "Reference data file : " << filename_ref << endl;

  }
  else{
    cout << "Incorrect usage!" << endl << endl;
    Print_opt();
    return 0;
  }

  string line_out, line_ref;
  //max 1000 events
  stringstream ref_event[1000], print_ref_event[1000];
  uint32_t ref_idnw[1000], out_idnw[1000];
  stringstream out_event[1000], print_out_event[1000];
  
  // CHECK the number of events for the two files and parse them in sstreams --------  
  while (!Ref_file.eof()){  

    //read line
    // If want to decode things maybe better to remove endl in sstreams
    getline(Ref_file, line_ref);
    ref_event[REF_EVENTS] << line_ref;
    print_ref_event[REF_EVENTS] << line_ref << endl;

    // If end of event
    if(line_ref[0] == '#'){
      ref_idnw[REF_EVENTS] = (uint32_t) stol(&line_ref[1], NULL, 16);
      REF_EVENTS += 1;
    }
  }
  
  Ref_file.close();

  while (!Out_file.eof()){  

    //read lines
    // If want to decode things maybe better to remove endl in sstreams
    getline(Out_file, line_out);
    out_event[OUT_EVENTS] << line_out;
    print_out_event[OUT_EVENTS] << line_out << endl;

    // If end of event
    if(line_out[0] == '#'){
      out_idnw[OUT_EVENTS] = (uint32_t) stol(&line_out[1], NULL, 16);
      OUT_EVENTS += 1;
      if (OUT_EVENTS ==1000) break;
    }
  }
  
  Out_file.close();
  //-----------------------------------------------------------------------------------

  // error flag
  bool ev_err = false, err= false;
  //error sstream
  stringstream error_ss;

  if(REF_EVENTS == OUT_EVENTS) {
    cout << endl << "Received " << REF_EVENTS << " events as expected" << endl;
    for (int i=1; i<=REF_EVENTS; i++){
      ev_err = false;
      // check event id
      cout << "\nEvent " << i << " :" << endl;
      error_ss << "\nEvent " << i << " :" << endl;
      int received_id = ((out_idnw[i-1])>>16) & 0xffff;
      int exp_id = ((ref_idnw[i-1])>>16) & 0xffff;
      if ( received_id == exp_id )
        cout << "  Correct :)  --  Received event ID = " << received_id << endl;
      else{
        cout << "  Uncorrect :(  --  Received event ID = " << received_id <<   ", reference one is " << exp_id << endl;
        ev_err = true;
        err = true;
        error_ss << "Uncorrect :(  --  Received event ID = " << received_id << ", reference one is " << exp_id << endl;
      }
      // Received event num_words
      int received_words= (out_idnw[i-1]) & 0xffff;
      int exp_words = (ref_idnw[i-1]) & 0xffff;
      if ( received_words == exp_words )
        cout << "  Correct :)  --  Received event words = " << received_words << endl;
      else{
        cout << "  Uncorrect :(  --  Received event words = " << received_words <<   ", reference one is " << exp_words << endl;
        ev_err = true;
        err = true;
        error_ss << "Uncorrect :(  --  Received event words = " << received_words << ", reference one is " << exp_words << endl;
      }
      // Received event content
      if ( (out_event[i-1]).str() == (ref_event[i-1]).str() )
        cout << "  Correct :)  --  Received event content is as expected" << endl;
      else{
        cout << "  Uncorrect :(  --  Received event content is not as expected, see report file" << endl;
        ev_err = true;
        err = true;
      }

      if(ev_err)  error_ss << endl << "Received Event " << i << " content :" << endl << (print_out_event[i-1]).str() << endl << endl << "Expected event " << i << " content:" << endl << (print_ref_event[i-1]).str() << endl;
    }
  }
  else if (REF_EVENTS > OUT_EVENTS){
    cout << endl << "Received " << OUT_EVENTS << " events while expected " << REF_EVENTS << " :(, see report for event contents" << endl;
    for (int i=1; i<=REF_EVENTS; i++){
      ev_err = false;
      if (i > OUT_EVENTS){
        cout << "\nMissing event " << i << " :(, see report for expected event content"<< endl << endl;
        error_ss << endl << "Expected event " << i << " content:" << endl << (out_event[i-1]).str() << endl << endl << endl;
        continue;
      }
      // check event id
      cout << "\nEvent " << i << " :" << endl;
      error_ss << "\nEvent " << i << " :" << endl;
      int received_id = ((out_idnw[i-1])>>16) & 0xffff;
      int exp_id = ((ref_idnw[i-1])>>16) & 0xffff;
      if ( received_id == exp_id )
        cout << "  Correct :)  --  Received event ID = " << received_id << endl;
      else{
        cout << "  Uncorrect :(  --  Received event ID = " << received_id <<   ", reference one is " << exp_id << endl;
        ev_err = true;
        err = true;
        error_ss << "Uncorrect :(  --  Received event ID = " << received_id << ", reference one is " << exp_id << endl;
      }
      // Received event num_words
      int received_words= (out_idnw[i-1]) & 0xffff;
      int exp_words = (ref_idnw[i-1]) & 0xffff;
      if ( received_words == exp_words )
        cout << "  Correct :)  --  Received event words = " << received_words << endl;
      else{
        cout << "  Uncorrect :(  --  Received event words = " << received_words <<   ", reference one is " << exp_words << endl;
        ev_err = true;
        err = true;
        error_ss << "Uncorrect :(  --  Received event words = " << received_words << ", reference one is " << exp_words << endl;
      }
      // Received event content
      if ( (out_event[i-1]).str() == (ref_event[i-1]).str() )
        cout << "  Correct :)  --  Received event content is as expected" << endl;
      else{
        cout << "  Uncorrect :(  --  Received event content is not as expected, see report file" << endl;
        ev_err = true;
        err = true;
      }

      if(ev_err) error_ss << endl << "Received Event " << i << " content :" << endl << (print_out_event[i-1]).str() << endl << endl << "Expected event " << i << " content:" << endl << (print_ref_event[i-1]).str() << endl;
    }
  }
  else{
    cout << endl << "Received " << OUT_EVENTS << " events while expected " << REF_EVENTS << " :C, see report for event contents" << endl;
    for (int i=1; i<=REF_EVENTS; i++){
      ev_err = false;
      // check event id
      cout << "\nEvent " << i << " :" << endl;
      error_ss << "\nEvent " << i << " :" << endl;
      int received_id = ((out_idnw[i-1])>>16) & 0xffff;
      int exp_id = ((ref_idnw[i-1])>>16) & 0xffff;
      if ( received_id == exp_id )
        cout << "  Correct :)  --  Received event ID = " << received_id << endl;
      else{
        cout << "  Uncorrect :(  --  Received event ID = " << received_id <<   ", reference one is " << exp_id << endl;
        ev_err = true;
        err = true;
        error_ss << "Uncorrect :(  --  Received event ID = " << received_id << ", reference one is " << exp_id << endl;
      }
      // Received event num_words
      int received_words= (out_idnw[i-1]) & 0xffff;
      int exp_words = (ref_idnw[i-1]) & 0xffff;
      if ( received_words == exp_words )
        cout << "  Correct :)  --  Received event words = " << received_words << endl;
      else{
        cout << "  Uncorrect :(  --  Received event words = " << received_words <<   ", reference one is " << exp_words << endl;
        ev_err = true;
        err = true;
        error_ss << "Uncorrect :(  --  Received event words = " << received_words << ", reference one is " << exp_words << endl;
      }
      // Received event content
      if ( (out_event[i-1]).str() == (ref_event[i-1]).str() )
        cout << "  Correct :)  --  Received event content is as expected" << endl;
      else{
        cout << "  Uncorrect :(  --  Received event content is not as expected, see report file" << endl;
        ev_err = true;
        err = true;
      }
      if(ev_err) error_ss << endl << "Received Event " << i << " content :" << endl << (print_out_event[i-1]).str() << endl << endl << "Expected event " << i << " content:" << endl << (print_ref_event[i-1]).str() << endl;
    }
    error_ss << endl << "content of exceedings events : ";
    for (int j = REF_EVENTS+1; j <= OUT_EVENTS; ++j)
    {
      error_ss << endl << "\nReceived Event " << j << " content :" << endl << (print_out_event[j-1]).str() << endl << endl;
    }
  }

  if (err){
    remove("Compare-Err-log.txt");
    ofstream Error_log("Compare-Err-log.txt");
  
    Error_log << "--------------------------------------------------------------\n\n";
    Error_log << error_ss.str() << std::endl;
    Error_log << "--------------------------------------------------------------\n\n";

    Error_log.close();
    
    cout << endl;
    return 1;
  }
  else
    cout << endl;
    return 0;    
}
