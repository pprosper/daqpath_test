#!/bin/sh

args=("$@")

if [ $# -eq 7 ]
then

cd tools
./DAQPATH_TEST_FilesGen $2 $3 $4 $5 $6 $7

if [ $2 -ge 17 ]
then
  rm ../addrtab/emp_daqpath_ex_design.xml
  mv DTC_DAQPATH.xml ../addrtab/emp_daqpath_ex_design.xml
fi
 
cd ..
./bin/DAQPATH_IPBUS_TEST ipbus my_connections.xml $1 tools/HW_IPBUS_TEST_DATA.txt $5 $6
cd tools
./DAQPATH_TEST_FilesComp Data_out_HW.txt Data_out_ref.txt
cd ..

else

echo "Pass to script 7 arguments : fpga_id NUM_CHANNELS NUM_EVENTS FW_FREQUENCY HDR_EN CH_MASK INPUT_DATA_WIDTH"
echo ""
echo "fpga_id : (x0/x1)"
echo ""
echo "NUM_CHANNELS : 2-64"
echo ""
echo "NUM_EVENTS : 1-100"
echo ""
echo "FW_FREQUENCY : clk_p frequency in MHz (e.g 360)"
echo ""
echo "HDR_EN : 0/1"
echo ""
echo "CH_MASK : 0000000000000000 - ffffffffffffffff - Max 64 bit vector in hex code : if ch_mask(i) = 1, channel i is skipped (MSB at left) --> ff00 masks channels 8-15 and enable channels 0-7"
echo ""
echo "INPUT_DATA_WIDTH : 8,16,32,64 - Daqpath input fifos width"
echo ""

fi
