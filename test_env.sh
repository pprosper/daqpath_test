#!/bin/sh

export LC_ALL=en_US.utf8 LANG=en_US.utf8
export PATH=/opt/cactus/bin:$PATH
export LD_LIBRARY_PATH=/opt/cactus/lib:$PWD/lib:$LD_LIBRARY_PATH