#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <ctime>

#include "uhal/uhal.hpp"

using namespace std;

int main(int argc, char* argv[])
{
  uhal::setLogLevelTo(uhal::WarningLevel());
  
  std::string lConnectionFilePath, lDeviceId;

  if (argc != 3)  {std::cout << "Usage : DAQPATH_IPBUS_READ <path_to_connection_file> <dev_id>"; return 1;}

  lConnectionFilePath = argv[1];
  lDeviceId = argv[2];

  // PART 2: Creating the HwInterface
  uhal::ConnectionManager lConnectionMgr("file://" + lConnectionFilePath);
  uhal::HwInterface lHW = lConnectionMgr.getDevice(lDeviceId);

  lHW.getNode("payload.DAQPATH.MAIN_CSR.C0").write(0x80008000);
  lHW.dispatch();
  lHW.getNode("payload.DAQPATH.MAIN_CSR.C0").write(0x80008050);
  lHW.dispatch();

  int NUM_EVENTS = 1000;
  
  remove ("tools/Data_out_HW.txt");

  std::cout << std::endl << "Reading from OUT0 FIFO" << std::endl << std::endl;
  
  std::clock_t initial_time = std::clock();

  int received_events = 0;
  // read OUT FIFO empty information from SREG0 
  uhal::ValWord< uint32_t > Out0_fifo_Empty_Reg = lHW.getNode("payload.DAQPATH.MAIN_CSR.S0").read();
  lHW.dispatch();

  int global_secs = 0, evt_ack = 0;

  while (received_events < NUM_EVENTS)
  {
   
    if (Out0_fifo_Empty_Reg & 0x00000003){   
    std::clock_t start = std::clock();
    
      while( ((std::clock() - start)/(double) CLOCKS_PER_SEC ) < 10 ){

        Out0_fifo_Empty_Reg = lHW.getNode("payload.DAQPATH.MAIN_CSR.S0").read();
        lHW.dispatch();

        if ((Out0_fifo_Empty_Reg & 0x00000003) == 0x0) {
          double secs = (std::clock() - initial_time)/((double) CLOCKS_PER_SEC);
          std::cout << "Event received at " << std::dec << std::fixed << std::setprecision(2) << secs << "s. Reading from output buffer\n";
          evt_ack = 1;
          break;
        }
        else {
          //int sec_from_init = (int) (std::clock() - initial_time)/((double) CLOCKS_PER_SEC);
          if ((int) (std::clock() - initial_time)/((double) CLOCKS_PER_SEC) > global_secs) 
            std::cout << "Listening from output buffer for " << std::dec << global_secs++ << "s.\n";
        }
      }
    }
    else {
      evt_ack = 1;
    }
    
    // Check if evt arrived when exiting while cycle above
    if (evt_ack == 0){
      std::cout << "No more events received. Max wait time reached : Closing interface\n\n";
      return 1;
    }
    else {
      evt_ack = 0;
    }

    uhal::ValWord< uint32_t > lReadDWL, lReadDWH;
    uhal::ValWord< uint32_t > lReadIDNW;
    size_t words;
    lReadIDNW = lHW.getNode("payload.DAQPATH.OUT.LINK0.IDNW_FIFO").read();
    lHW.dispatch();

    ofstream out_file;
    out_file.open("tools/Data_out_HW.txt", ios::out | ios::app);
    std::stringstream oss;    

    std::cout << "Event " << (int) ((lReadIDNW >> 16) & 0xFFFF) << " received. Writing data into text file." << std::endl;

    words = (size_t) (lReadIDNW & 0xFFFF);
    std::cout << "Received words : " << std::dec <<  words << std::endl;

    std::cout << "\nFIFO content : \n";
    for (size_t i = 0; i < words; ++i)
    {
      lReadDWL = lHW.getNode("payload.DAQPATH.OUT.LINK0.DWL_FIFO").read();
      lHW.dispatch();
      lReadDWH = lHW.getNode("payload.DAQPATH.OUT.LINK0.DWH_FIFO").read();
      lHW.dispatch();
      std::cout << std::hex << std::setw(8) << std::setfill('0') << lReadDWH.value() ;
      std::cout << std::hex << std::setw(8) << std::setfill('0') << lReadDWL.value() << std::endl ;
      oss << std::hex << std::setw(8) << std::setfill('0') << lReadDWH.value() ;
      oss << std::hex << std::setw(8) << std::setfill('0') << lReadDWL.value() << std::endl;
    }
    
    oss << "#" << std::hex << std::setw(8) << std::setfill('0') << lReadIDNW.value() << std::endl;

    out_file << oss.str();

    received_events++;

    if ( received_events <= NUM_EVENTS ) std::cout << std::endl << "Event " << (int) ((lReadIDNW >> 16) & 0xFFFF) << " received and written into tools/Data_out.txt file" << std::endl << std::endl;
    
    if ( received_events == NUM_EVENTS ) {
      std::clock_t start = std::clock();
      while( ((std::clock() - start)/(double) CLOCKS_PER_SEC ) < 0.5 );
    
      Out0_fifo_Empty_Reg = lHW.getNode("payload.DAQPATH.MAIN_CSR.S0").read();
      lHW.dispatch();

      if ((Out0_fifo_Empty_Reg & 0x00000003) == 0x00000003)
        std::cout << "No more events stored in OUT FIFOs... : \n";
      else
        std::cout << "More events stored in OUT FIFOs... : \n";      
     
      std::cout << "All transmitted events received : closing interface" << std::endl << std::endl;
    }

    Out0_fifo_Empty_Reg = lHW.getNode("payload.DAQPATH.MAIN_CSR.S0").read();
    lHW.dispatch();

  }

  return 0;
}