#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <ctime>

#include "uhal/uhal.hpp"

using namespace std;

int main(int argc, char* argv[])
{
  uhal::setLogLevelTo(uhal::WarningLevel());
  
  std::string lConnectionFilePath, lDeviceId;

  if (argc != 5)  {std::cout << "Usage : DAQPATH_IPBUS_SETTINGS <path_to_connection_file> <dev_id> <hdr_en> <ch_mask>"; return 1;}

  unsigned int ch_mask_low=0, ch_mask_high = 0;
  int hdr_en = 1;

  lConnectionFilePath = argv[1];
  lDeviceId = argv[2];
  hdr_en = stoi(argv[3],NULL,10);
  size_t length = strlen(argv[4]);
  if (length <= 8){
    ch_mask_low = stol(argv[4], NULL, 16);
    ch_mask_high = 0x0;
  }
  else{
    char ch_low[4];
    char* ch_high = new char[length-8+1];
    memcpy(ch_low, &argv[4][length-8], 8);
    memcpy(ch_high, &argv[4][0], length-8);
    ch_mask_low = stol(ch_low, NULL, 16);
    ch_mask_high = stol(ch_high, NULL, 16);
  }

  // PART 2: Creating the HwInterface
  uhal::ConnectionManager lConnectionMgr("file://" + lConnectionFilePath);
  uhal::HwInterface lHW = lConnectionMgr.getDevice(lDeviceId);

  //Header Enable and Channel Mask Settings----------------------------------
  //------------------------------------------------
  std::cout << std::endl << "HDR_ENABLE:(" << hdr_en << ") ->  Writing DAQPATH CTRL0 register" << std::endl << std::endl;
  lHW.getNode("payload.DAQPATH.DAQPATH_CSR.C0").write(hdr_en);
  lHW.dispatch();

  std::cout << std::endl << "Channel Mask Settings: (" << std::hex << ch_mask_low + ch_mask_high*0x100000000  << ") -> Writing DAQPATH ch_mask registers" << std::endl << std::endl;
  lHW.getNode("payload.DAQPATH.DAQPATH_CSR.C1").write((uint32_t)ch_mask_low);
  lHW.dispatch();
  lHW.getNode("payload.DAQPATH.DAQPATH_CSR.C2").write((uint32_t)ch_mask_high);
  lHW.dispatch();
  //------------------------------------------------

  return 0;
}