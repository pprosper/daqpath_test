#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <ctime>

#include "uhal/uhal.hpp"

using namespace std;

void Print_usage(){
  std::cout << "\n\nIncorrect usage!" << std::endl << std::endl;
  std::cout << "First argument must be : " << std::endl;
  std::cout << "1) \"ipbus\" - if you want to inject data from a text file into IPBUS input FIFOs" << std::endl;
  std::cout << "2) \"evtgen\" - if you want to use internal pseudo-random event generator to inject data into input FIFOs" << std::endl << std::endl;
  std::cout << "Then the usage should be :" << std::endl;
  std::cout << "1) ipbus  : DAQPATH_IPBUS_TEST <ipbus>  <path_to_connection_file> <device_id> <path_to_ipbus_data_file> <hdr_en> <ch_mask>" << std::endl;
  std::cout << "2) evtgen : DAQPATH_IPBUS_TEST <evtgen> <path_to_connection_file> <device_id> <NUM_CHANNELS> <NUM_EVENTS> <NUM_WORDS> <DELTA_T> <hdr_en> <ch_mask>" << std::endl;
  std::cout << "info : " << std::endl << "  hdr_en      : 1 if you want headers, 0 if not" << std::endl;
  std::cout << "  ch_mask(i)  : 1 if you want to mask channel i, 0 if not - MSB at left (e.g. ff00 channels 8-15 masked, 0-7 unmasked)" << std::endl;
  return;
}

int main(int argc, char* argv[])
{
  uhal::setLogLevelTo(uhal::WarningLevel());
  
  // PART 1: Argument parsing
  if ((argc < 4) || (std::string(argv[1]) != "ipbus" && std::string(argv[1]) != "evtgen")){
    std::cout << "\n Error when parsing arguments : See usage\n";
    Print_usage();
    return 0;
  }

  const int NUM_MAIN_REGISTERS = 8;
  const int NUM_CH_REGISTERS = 4;
  std::string lConnectionFilePath, lDeviceId, lDataFilePath;
  //default values
  int NUM_CHANNELS = 8;
  int NUM_EVENTS = 2;
  int NUM_WORDS = 2;
  long int DELTA_T = 0x1000;
  unsigned int ch_mask_low=0, ch_mask_high = 0;
  int hdr_en = 1;

  bool IPB=false;

  if (std::string(argv[1]) == "ipbus")
    IPB=true;

  if (IPB){
    if (argc != 7)  {Print_usage();return 0;}
    else{
      lConnectionFilePath = argv[2];
      lDeviceId = argv[3];
      lDataFilePath = argv[4];
      hdr_en = stoi(argv[5],NULL,10);
      size_t length = strlen(argv[6]);
      if (length <= 8){
        ch_mask_low = stol(argv[6], NULL, 16);
        ch_mask_high = 0x0;
      }
      else{
  char ch_low[9];
        char* ch_high = new char[length-8+1];
        memcpy(ch_low, &argv[6][length-8], 8);
        memcpy(ch_high, &argv[6][0], length-8);
        ch_mask_low = stol(ch_low, NULL, 16);
        ch_mask_high = stol(ch_high, NULL, 16);
      }
    }
  }
  else{
    if (argc != 10)  {Print_usage(); return 0;}
    else{
      lConnectionFilePath = argv[2];
      lDeviceId = argv[3];
      NUM_CHANNELS = stoi(argv[4],NULL,10);
      NUM_EVENTS = stoi(argv[5],NULL,10);
      NUM_WORDS = stoi(argv[6],NULL,10);
      DELTA_T = stol(argv[7],NULL,10);
      hdr_en = stoi(argv[8],NULL,10);
      size_t length = strlen(argv[9]);
      if (length <= 8){
        ch_mask_low = stol(argv[9], NULL, 16);
        ch_mask_high = 0x0;
      }
      else{
        char ch_low[9];
        char* ch_high = new char[length-8+1];
        memcpy(ch_low, &argv[9][length-8], 8);
        memcpy(ch_high, &argv[9][0], length-8);
        ch_mask_low = stol(ch_low, NULL, 16);
        ch_mask_high = stol(ch_high, NULL, 16);
      }
      //Maximum OUT FIFO depth check
      if ((NUM_WORDS + 1)*NUM_CHANNELS*NUM_EVENTS > 1024) {std::cout << "\nPlease reduce number of events/words to don't fill output fifos\n\n"; return 0;}
    }
  }

  // PART 2: Creating the HwInterface
  uhal::ConnectionManager lConnectionMgr("file://" + lConnectionFilePath);
  uhal::HwInterface lHW = lConnectionMgr.getDevice(lDeviceId);

  // PART 3: Writing CTRL REGISTERS
  std::cout << "Reset the global system... " << std::endl;
  lHW.getNode("payload.DAQPATH.MAIN_CSR.C0").write(0x00000000);
  lHW.dispatch();

  for (int i = 0; i < NUM_MAIN_REGISTERS; ++i)
  {
    std::cout << "Writing into main CTRL register " << std::to_string(i) << "..." << std::endl;
    uint32_t CTRL_REG_DATA = 0x0;
    CTRL_REG_DATA = 0x80000000 + 0x1000000 * i;
    if (i == 0) CTRL_REG_DATA += 0x8000;
    lHW.getNode("payload.DAQPATH.MAIN_CSR.C" + std::to_string(i)).write(CTRL_REG_DATA);
    lHW.dispatch();
  }

  //num_channels and num_event retrival
  if (IPB){

    ifstream lDataFile;
    lDataFile.open(lDataFilePath, ios::in);

    if(!lDataFile.is_open()){
      std::cout << "Error opening file : aborting";
      return 1;
    }

    // events and channels number retrival from data text file
    std::string line;
    std::stringstream iss;

    getline(lDataFile, line);
    iss.str(line);
    iss >> NUM_CHANNELS >> NUM_EVENTS;
    std::cout << std::dec << NUM_CHANNELS << " " << NUM_EVENTS << std::hex << std::endl;
    lDataFile.close();

  }


  for (int i = 0; i < NUM_CHANNELS; ++i)
    for (int j = 0; j < NUM_CH_REGISTERS; ++j)
    {
      std::cout << "Writing into channel" << std::to_string(i) << " CTRL register " << std::to_string(j) << "..." << std::endl;
      uint32_t CTRL_REG_DATA;

      switch (j)
      {

        case 1 : CTRL_REG_DATA = DELTA_T; break;
        case 2 : CTRL_REG_DATA = NUM_EVENTS; break;
        case 3 : CTRL_REG_DATA = NUM_WORDS; break;

        default : CTRL_REG_DATA = 0xC0000000 + 0x1000000 * j + 0x10000 * i; break;
      }

      lHW.getNode("payload.DAQPATH.CH_CSR.CH" + std::to_string(i) + ".C" + std::to_string(j)).write(CTRL_REG_DATA);
      lHW.dispatch();
    }

  // PART 3: Reading CTRL/STATUS REGISTERS

  std::cout << "MAIN REGISTERS : " << std::endl << std::endl;

  for (int i = 0; i < NUM_MAIN_REGISTERS; ++i)
  {
    uhal::ValWord<uint32_t> lReg = lHW.getNode("payload.DAQPATH.MAIN_CSR.C" + std::to_string(i)).read();
    lHW.dispatch();
    std::cout << "CREG" << std::to_string(i) << " VALUE : " << std::hex << std::setw(8) << std::setfill('0') << lReg.value() << std::endl;

    lReg = lHW.getNode("payload.DAQPATH.MAIN_CSR.S" + std::to_string(i)).read();
    lHW.dispatch();
    std::cout << "SREG" << std::to_string(i) << " VALUE : " << std::hex << std::setw(8) << std::setfill('0') << lReg.value() << std::endl;
  }

  for (int j = 0; j < NUM_CHANNELS; ++j)
  {

    std::cout << std::endl << "CHANNEL" << std::to_string(j) <<" REGISTERS : " << std::endl << std::endl;

    for (int i = 0; i < NUM_CH_REGISTERS; ++i)
    {
      uhal::ValWord<uint32_t> lReg = lHW.getNode("payload.DAQPATH.CH_CSR.CH" + std::to_string(j) + ".C" + std::to_string(i)).read();
      lHW.dispatch();
      std::cout << "CREG" << std::to_string(i) << " VALUE : " << std::hex << std::setw(8) << std::setfill('0') << lReg.value() << std::endl;

      lReg = lHW.getNode("payload.DAQPATH.CH_CSR.CH" + std::to_string(j) + ".S" + std::to_string(i)).read();
      lHW.dispatch();
      std::cout << "SREG" << std::to_string(i) << " VALUE : " << std::hex << std::setw(8) << std::setfill('0') << lReg.value() << std::endl;
    }
  }

  if (IPB)
  {
    // writing INPUT FIFOS

    std::cout << "Writing data into IPBUS input FIFOs" << std::endl << std::endl;
    
    ifstream lDataFile;
    lDataFile.open(lDataFilePath, ios::in);
 
    if(!lDataFile.is_open()){
      std::cout << "Error opening file : aborting";
      return 1;
    }

    std::string line;
    std::stringstream iss;

    getline(lDataFile, line);  
    
    for(int i = 0; i < NUM_EVENTS; i++)
    {
      std::cout << "Writing event "<< i+1 <<" data into input IPB FIFOs ..." << std::endl;
    
      for (int j = 0; j <= NUM_CHANNELS - 1; ++j)
      {

        uint32_t TS;
        uint32_t N_WORDS, EV_ID;

        getline(lDataFile, line);
        if(line[0] == '-'){
          std::cout << "Event " << i+1 << " is missing for channel " << j << std::endl;
          continue;
        }

        TS = (uint32_t) stol(line.substr(0,8), 0, 16);
        EV_ID = (uint32_t) stoi(line.substr(9,4), 0, 16);
        N_WORDS = (uint32_t) stoi(line.substr(14,4), 0, 16);
        std::cout << "Writing event " << (uint32_t) EV_ID << " with " << (uint32_t) N_WORDS << " words into IPB INPUT FIFO " << j  << " at Time stamp " << std::hex << std::setw(8) << std::setfill('0') << (uint32_t)TS << std::endl;

        std::string ch_addr = "payload.DAQPATH.IN.CH" + std::to_string(j) + ".";

        lHW.getNode(ch_addr + "TS_FIFO").write( TS );
        lHW.dispatch();
        lHW.getNode(ch_addr + "ID_FIFO").write( EV_ID );
        lHW.dispatch();
        lHW.getNode(ch_addr + "NW_FIFO").write( N_WORDS );
        lHW.dispatch();

        for(unsigned int k = 0; k < N_WORDS; ++k)
        {
          getline(lDataFile, line);
          uint32_t DWH, DWL;

          DWH = (uint32_t) stol(line.substr(0,8), 0, 16);
          DWL = (uint32_t) stol(line.substr(8,8), 0, 16);

          std::cout << "data word " << k << " : "  << std::hex << std::setw(8) << std::setfill('0') << DWH;
          std::cout << std::hex << std::setw(8) << std::setfill('0') << DWL << std::endl;

          lHW.getNode(ch_addr + "DWH_FIFO").write( DWH );
          lHW.dispatch();
          lHW.getNode(ch_addr + "DWL_FIFO").write( DWL );
          lHW.dispatch();
        }
      }      
     
      if (i == NUM_EVENTS-1)    std::cout << std::endl << "Waiting for events  to be written to IPBUS FIFOs...";

    } 

    std::clock_t start = std::clock();
    while( ((std::clock() - start)/(double) CLOCKS_PER_SEC ) < 0.5 );
  
    //Header Enable and Channel Mask Settings----------------------------------
    //------------------------------------------------
    std::cout << std::endl << "HDR_ENABLE:(" << hdr_en << ") ->  Writing DAQPATH CTRL0 register" << std::endl << std::endl;
    lHW.getNode("payload.DAQPATH.DAQPATH_CSR.C0").write(hdr_en);
    lHW.dispatch();

    std::cout << std::endl << "Channel Mask Settings: (" << std::hex << std::setw(ceil(NUM_CHANNELS/4)) << std::setfill('0') << ch_mask_low + ch_mask_high*0x100000000  << ") -> Writing DAQPATH ch_mask registers" << std::endl << std::endl;
    lHW.getNode("payload.DAQPATH.DAQPATH_CSR.C1").write((uint32_t)ch_mask_low);
    lHW.dispatch();
    lHW.getNode("payload.DAQPATH.DAQPATH_CSR.C2").write((uint32_t)ch_mask_high);
    lHW.dispatch();
    //------------------------------------------------

    start = std::clock();
    while( ((std::clock() - start)/(double) CLOCKS_PER_SEC ) < 0.5 );
    
    // DAQPATH enable ----------------------------------------------
    std::cout << std::endl << "DAQPATH_EN = 1 : Writing Main CTRL0 register" << std::endl << std::endl;
    lHW.getNode("payload.DAQPATH.MAIN_CSR.C0").write(0xC0008050);
    lHW.dispatch();
    //--------------------------------------------------------------

  } 

  else{

    std::clock_t start = std::clock();
    while( ((std::clock() - start)/(double) CLOCKS_PER_SEC ) < 0.5 );

    //Header Enable and Channel Mask Settings----------------------------------
    //------------------------------------------------
    std::cout << std::endl << "HDR_ENABLE:(" << hdr_en << ") ->  Writing DAQPATH CTRL0 register" << std::endl << std::endl;
    lHW.getNode("payload.DAQPATH.DAQPATH_CSR.C0").write(hdr_en);
    lHW.dispatch();

    std::cout << std::endl << "Channel Mask Settings: (" << std::hex << std::setw(ceil(NUM_CHANNELS/4)) << std::setfill('0') << ch_mask_low + ch_mask_high*0x100000000  << ") -> Writing DAQPATH ch_mask registers" << std::endl << std::endl;
    lHW.getNode("payload.DAQPATH.DAQPATH_CSR.C1").write((uint32_t)ch_mask_low);
    lHW.dispatch();
    lHW.getNode("payload.DAQPATH.DAQPATH_CSR.C2").write((uint32_t)ch_mask_high);
    lHW.dispatch();
    //------------------------------------------------

    std::cout << std::endl << "TRGGEN_EN = 1 : Writing Main CTRL0 register" << std::endl << std::endl;
    lHW.getNode("payload.DAQPATH.MAIN_CSR.C0").write(0xC0008001);
    lHW.dispatch();

    std::cout << std::endl << "EVTGEN_EN = 1 : Writing Main CTRL0 register" << std::endl << std::endl;
    lHW.getNode("payload.DAQPATH.MAIN_CSR.C0").write(0xC0008005);
    lHW.dispatch();
    
    std::cout << std::endl << "DAQPATH_EN = 1 : Writing Main CTRL0 register" << std::endl << std::endl;
    lHW.getNode("payload.DAQPATH.MAIN_CSR.C0").write(0xC0008015);
    lHW.dispatch();

  }

  remove ("tools/Data_out_HW.txt");

  std::cout << std::endl << "Reading from Main/Channel status registers" << std::endl << std::endl;

  std::cout << "MAIN REGISTERS : " << std::endl << std::endl; 

  for (int i = 0; i < NUM_MAIN_REGISTERS; ++i)
  {
    uhal::ValWord<uint32_t> lReg = lHW.getNode("payload.DAQPATH.MAIN_CSR.S" + std::to_string(i)).read();
    lHW.dispatch();
    std::cout << "SREG" << std::to_string(i) << " VALUE : " << std::hex << std::setw(8) << std::setfill('0') << lReg.value() << std::endl;
  }

  for (int j = 0; j < NUM_CHANNELS; ++j)
  {

  std::cout << std::endl << "CHANNEL" << std::to_string(j) <<" REGISTERS : " << std::endl << std::endl;

  for (int i = 0; i < NUM_CH_REGISTERS; ++i)
  {
    uhal::ValWord<uint32_t> lReg = lHW.getNode("payload.DAQPATH.CH_CSR.CH" + std::to_string(j) + ".S" + std::to_string(i)).read();
    lHW.dispatch();
    std::cout << "SREG" << std::to_string(i) << " VALUE : " << std::hex << std::setw(8) << std::setfill('0') << lReg.value() << std::endl;
  }
  }

  std::cout << std::endl << "Reading from OUT0 FIFO" << std::endl << std::endl;

  std::clock_t initial_time = std::clock();

  int received_events = 0;
  // read OUT FIFO empty information from SREG0 
  uhal::ValWord< uint32_t > Out0_fifo_Empty_Reg = lHW.getNode("payload.DAQPATH.MAIN_CSR.S0").read();
  lHW.dispatch();

  int global_secs = 0, evt_ack = 0;

  while (received_events < NUM_EVENTS)
  {
   
    if (Out0_fifo_Empty_Reg & 0x00000003){   
    std::clock_t start = std::clock();
    
      while( ((std::clock() - start)/(double) CLOCKS_PER_SEC ) < 10 ){

        Out0_fifo_Empty_Reg = lHW.getNode("payload.DAQPATH.MAIN_CSR.S0").read();
        lHW.dispatch();

        if ((Out0_fifo_Empty_Reg & 0x00000003) == 0x0) {
          double secs = (std::clock() - initial_time)/((double) CLOCKS_PER_SEC);
          std::cout << "Event received at " << std::dec << std::fixed << std::setprecision(2) << secs << "s. Reading from output buffer\n";
          evt_ack = 1;
          break;
        }
        else {
          //int sec_from_init = (int) (std::clock() - initial_time)/((double) CLOCKS_PER_SEC);
          if ((int) (std::clock() - initial_time)/((double) CLOCKS_PER_SEC) > global_secs) 
            std::cout << "Listening from output buffer for " << std::dec << global_secs++ << "s.\n";
        }
      }
    }
    else {
      evt_ack = 1;
    }
    
    // Check if evt arrived when exiting while cycle above
    if (evt_ack == 0){
      std::cout << "No more events received. Max wait time reached : Closing interface\n\n";
      return 1;
    }
    else {
      evt_ack = 0;
    }

    uhal::ValWord< uint32_t > lReadDWL, lReadDWH;
    uhal::ValWord< uint32_t > lReadIDNW;
    size_t words;
    lReadIDNW = lHW.getNode("payload.DAQPATH.OUT.LINK0.IDNW_FIFO").read();
    lHW.dispatch();

    ofstream out_file;
    out_file.open("tools/Data_out_HW.txt", ios::out | ios::app);
    std::stringstream oss;    

    std::cout << "Event " << (int) ((lReadIDNW >> 16) & 0xFFFF) << " received. Writing data into text file." << std::endl;

    words = (size_t) (lReadIDNW & 0xFFFF);
    std::cout << "Received words : " << std::dec <<  words << std::endl;

    std::cout << "\nFIFO content : \n";
    for (size_t i = 0; i < words; ++i)
    {
      lReadDWL = lHW.getNode("payload.DAQPATH.OUT.LINK0.DWL_FIFO").read();
      lHW.dispatch();
      lReadDWH = lHW.getNode("payload.DAQPATH.OUT.LINK0.DWH_FIFO").read();
      lHW.dispatch();
      std::cout << std::hex << std::setw(8) << std::setfill('0') << lReadDWH.value() ;
      std::cout << std::hex << std::setw(8) << std::setfill('0') << lReadDWL.value() << std::endl ;
      oss << std::hex << std::setw(8) << std::setfill('0') << lReadDWH.value() ;
      oss << std::hex << std::setw(8) << std::setfill('0') << lReadDWL.value() << std::endl;
    }
    
    oss << "#" << std::hex << std::setw(8) << std::setfill('0') << lReadIDNW.value() << std::endl;

    out_file << oss.str();

    received_events++;

    if ( received_events <= NUM_EVENTS ) std::cout << std::endl << "Event " << (int) ((lReadIDNW >> 16) & 0xFFFF) << " received and written into tools/Data_out.txt file" << std::endl << std::endl;
    
    if ( received_events == NUM_EVENTS ) {
      std::clock_t start = std::clock();
      while( ((std::clock() - start)/(double) CLOCKS_PER_SEC ) < 0.5 );
    
      Out0_fifo_Empty_Reg = lHW.getNode("payload.DAQPATH.MAIN_CSR.S0").read();
      lHW.dispatch();

      if ((Out0_fifo_Empty_Reg & 0x00000003) == 0x00000003)
        std::cout << "No more events stored in OUT FIFOs... : \n";
      else
        std::cout << "More events stored in OUT FIFOs... : \n";      
     
      std::cout << "All transmitted events received : closing interface" << std::endl << std::endl;
    }

    Out0_fifo_Empty_Reg = lHW.getNode("payload.DAQPATH.MAIN_CSR.S0").read();
    lHW.dispatch();

  }

  return 0;

}