#!/bin/sh

args=("$@")

if [ $# -eq 8 ]
then

  if [ $8 -ge 1 ]
  then
  
  rm tools/multiple_results.txt
  rm Multiple_test_error_log.txt

  for (( c=1; c<=$8; c++ ))
  do

  cd tools
  ./DAQPATH_TEST_FilesGen $2 $3 $4 $5 $6 $7
  
  if [ $2 -ge 17 ]
  then
    rm ../addrtab/emp_daqpath_ex_design.xml
    mv DTC_DAQPATH.xml ../addrtab/emp_daqpath_ex_design.xml
  fi
  
  cd ..
  ./bin/DAQPATH_IPBUS_TEST ipbus my_connections.xml $1 tools/HW_IPBUS_TEST_DATA.txt $5 $6
  cd tools
  ./DAQPATH_TEST_FilesComp Data_out_HW.txt Data_out_ref.txt
  
    if [ $? -eq 1 ]
    then
    echo "1" >> multiple_results.txt
    cat Compare-Err-log.txt >> ../Multiple_test_error_log.txt
    else
    echo "0" >> multiple_results.txt
    fi
 
  cd ..

  done

  cd tools
  ./multiple_tests_results_reader
  cd ..

  echo ""
  echo "Look at file Multiple_test_error_log.txt to check for errors if present";
  echo ""  

  else
  echo ""
  echo "Pass a number of tests argument (arg 8) greater than 0"
  echo ""  
  fi

else
echo ""
echo "Pass to script 8 arguments : fpga_id NUM_CHANNELS NUM_EVENTS FW_FREQUENCY HDR_EN CH_MASK INPUT_DATA_WIDTH NUM_TESTS"
echo ""
echo "fpga_id : (x0/x1)"
echo ""
echo "NUM_CHANNELS : 2-64"
echo ""
echo "NUM_EVENTS : 1-100"
echo ""
echo "FW_FREQUENCY : clk_p frequency in MHz (e.g 360)"
echo ""
echo "HDR_EN : 0/1"
echo ""
echo "CH_MASK : 0000000000000000 - ffffffffffffffff - Max 64 bit vector in hex code : if ch_mask(i) = 1, channel i is skipped (MSB at left) --> ff00 masks channels 8-15 and enable channels 0-7"
echo ""
echo "INPUT_DATA_WIDTH : 8,16,32,64 - Daqpath input fifos width"
echo ""
echo "NUM_TESTS : >0 - num tests to perform"
echo ""
fi
