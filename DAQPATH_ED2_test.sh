#!/bin/sh

args=("$@")

if [ $# -eq 1 ]
then

empbutler -c my_connections.xml do $1 reset internal
empbutler -c my_connections.xml do $1 buffers rx PlayOnce -c 0-15 --inject file://tools/DAQPATH_ED2/ldata_input.txt
./bin/DAQPATH_IPBUS_SETTINGS my_connections.xml $1 1 0
./bin/DAQPATH_IPBUS_READ my_connections.xml $1
cd tools
./DAQPATH_TEST_FilesComp Data_out_HW.txt DAQPATH_ED2/ldata_input_Data_out_ref.txt
cd ..

else

echo "Pass to script 1 argument : fpga_id"

fi
